import requests

from utils.issue import create_issue, assign_issue_to_epic


def create_epic_with_issues(base_url, headers, group_id, project_id, epic_info, issues):
    """
    Creates an epic with multiple issues associated with it.

    Args:
        base_url (str): The base URL of the project management API.
        headers (dict): The headers to include in the API requests, including authentication.
        group_id (str): The ID of the group under which the epic and issues will be created.
        project_id (int): The ID of the project under which the issues will be created.
        epic_info (dict): A dictionary containing the epic details (title, description, start_date, due_date).
        issues (list): A list of dictionaries, each containing issue details (title, description).

    Returns:
        None
    """
    epic = create_epic(base_url, headers, group_id, epic_info)
    epic_id = epic['id']
    print(f"Epic created: {epic}")

    for issue in issues:
        created_issue = create_issue(base_url, headers, project_id, issue, epic_id)
        print(f"Issue created: {created_issue}")
        assign_issue_to_epic(base_url, headers, group_id, created_issue["id"], epic_id)
        print(f"Issue {created_issue['id']} assigned to epic {epic_id}")


def create_epic(base_url, headers, group_id, epic_info):
    """
    Creates an epic.

    Args:
        base_url (str): The base URL of the project management API.
        headers (dict): The headers to include in the API requests, including authentication.
        group_id (str): The ID of the group under which the epic and issues will be created.
        epic_info (dict): A dictionary containing the epic details (title, description, start_date, due_date).

    Returns:
        None
    """
    url = f"{base_url}/groups/{group_id}/epics"
    payload = {
        "title": epic_info['title'],
        "description": epic_info['description'],
        "start_date_is_fixed": True,
        "start_date_fixed": epic_info['start_date'],
        "due_date_is_fixed": True,
        "due_date_fixed": epic_info['due_date'],
    }
    response = requests.post(url, headers=headers, json=payload)
    return response.json()
