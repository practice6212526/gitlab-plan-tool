class ProjectDict:
    def __init__(self, project_string):
        self._projects = {name: int(id) for name, id in (project.split(':') for project in project_string.split(','))}

    def __getattr__(self, item):
        return self._projects.get(item, None)

