import requests


def create_issue(base_url, headers, project_id, issue, epic_id):
    url = f"{base_url}/projects/{project_id}/issues"
    payload = {
        "title": issue['title'],
        "description": issue['description'],
        "epic_id": epic_id
    }
    response = requests.post(url, headers=headers, json=payload)
    return response.json()


def assign_issue_to_epic(base_url, headers, group_id, issue_id, epic_id):
    url = f"{base_url}/groups/{group_id}/epics/{epic_id}/issues/{issue_id}"
    response = requests.post(url, headers=headers)
    return response.json()
