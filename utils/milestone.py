import requests


def create_milestone(base_url, headers, project_id, milestone_info):
    url = f"{base_url}/groups/{project_id}/milestones"

    payload = {
        "title": milestone_info['title'],
        "description": milestone_info['description']
    }

    if milestone_info.get('start_date') is not None:
        payload['start_date'] = milestone_info['start_date']

    if milestone_info.get('due_date') is not None:
        payload['due_date'] = milestone_info['due_date']

    response = requests.post(url, headers=headers, json=payload)

    if response.status_code == 200:
        print(f"Milestone created: {milestone_info}")

    return response.json()


def create_sprints(base_url, headers, group_id, sprints):
    """
    Creates sprints (milestones) for project management.

    Args: base_url (str): The base URL of the project management API. headers (dict): The headers to include in the
    API requests, including authentication. group_id (int): The ID of the group under which the sprints will be
    created. sprints (list): A list of dictionaries, each containing sprint details (title, description, start_date,
    due_date).

    Returns:
        None
    """
    for sprint in sprints:
        create_milestone(base_url, headers, group_id, sprint)
