import os
from dotenv import load_dotenv

from gitlab.roadmap import main
from utils import ProjectDict


load_dotenv()

GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
GITLAB_BASE_URL = os.getenv('GITLAB_BASE_URL')
GROUP_ID = os.getenv('GROUP_ID')
EXAMPLE_PROJECT_ID = os.getenv('PROJECT_ID')
PROJECTS = os.getenv('PROJECTS')

if not all([GITLAB_ACCESS_TOKEN, GITLAB_BASE_URL, GROUP_ID, PROJECTS]):
    raise ValueError("Missing one or more required environment variables: GITLAB_ACCESS_TOKEN, GITLAB_BASE_URL, "
                     "PROJECT_ID")

# Run the main script
if __name__ == "__main__":
    projects_dict = ProjectDict(PROJECTS)  # convert string to dict
    headers = {
        "PRIVATE-TOKEN": GITLAB_ACCESS_TOKEN,
        "Content-Type": "application/json"
    }
    main(GITLAB_BASE_URL, GROUP_ID, projects_dict, headers)
