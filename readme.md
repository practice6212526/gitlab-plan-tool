# Project Management Automation Script

This project contains a script for automating the creation of epics, issues, sprints, and release milestones in a GitLab project. It leverages the GitLab API to streamline project management tasks.

## Features

- Create epics with associated issues.
- Create standalone epics without issues.
- Create sprints (milestones).
- Create release milestones for features.

## Requirements

- Python 3.11+
- GitLab account with access token
- GitLab project setup

## Setup

1. **Clone the repository**
2. **Setup environment:**
```
python -m venv venv
source vevn/bin/activate
pip install -r requirements.txt
```
Create a .env file in the root of your project and add the following variables:
```
GITLAB_ACCESS_TOKEN=your_gitlab_access_token
GITLAB_BASE_URL=https://gitlab.example.com/api/v4
GROUP_ID=your_group_id
PROJECTS=ProjectA:123,ProjectB:456,ProjectC:789
```

## Run

1. **Make changes in roadmap.py**
2. **Run app.py**
```python app.py```
