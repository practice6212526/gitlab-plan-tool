from utils.epic import create_epic_with_issues, create_epic
from utils.milestone import create_sprints, create_milestone


def main(base_url, group_id, projects, headers):
    """
    Main function to create epics, issues, sprints, and release milestones.

    Args:
        base_url (str): The base URL of the project management API.
        group_id (str): The ID of the group under which the tasks will be created.
        projects (ProjectDict): A dictionary mapping project names to their IDs.
        headers (dict): A dictionary mapping GitLab API headers to their values.

    Returns:
        None
    """


    ###############################
    #       Example Usage         #
    ###############################

    create_epic(base_url, headers, group_id, {
        'title': 'Metrics Feature',
        'description': 'Integrate metrics feature with UI and API.',
        'start_date': '2024-05-01',
        'due_date': '2024-06-01'
    })

    create_epic_with_issues(base_url, headers, group_id, projects.Tile,
                            {
                                'title': 'User Authentication Feature',
                                'description': 'Develop a new user authentication feature that includes user '
                                               'registration, login, password recovery, and two-factor authentication.',
                                'start_date': '2024-05-01',
                                'due_date': '2024-07-11'
                            }, [
                                {"title": "User Registration",
                                 "description": "Implement user registration functionality with email verification."},
                                {"title": "User Login",
                                 "description": "Implement user login functionality with email and password authentication."},
                                {"title": "Password Recovery",
                                 "description": "Implement password recovery functionality with email verification."},
                                {"title": "Two-Factor Authentication",
                                 "description": "Implement two-factor authentication using SMS or email."},
                                {"title": "Frontend Integration",
                                 "description": "Integrate the user authentication feature into the frontend application"},
                                {"title": "Backend Integration",
                                 "description": "Integrate the user authentication feature into the backend API."},
                                {"title": "Testing and QA",
                                 "description": "Conduct thorough testing and quality assurance for the user authentication feature."}
                            ]
                            )

    create_sprints(base_url, headers, group_id, [
        {"title": "Sprint 13", "description": "Thirteenth sprint for Production Deployment.",
         "start_date": "2024-06-28",
         "due_date": "2024-07-11"},
        {"title": "Sprint 14", "description": "Fourteenth sprint for Post-Deployment Support.",
         "start_date": "2024-07-12",
         "due_date": "2024-07-25"}
    ])

    create_milestone(base_url, headers, group_id, {
        'title': 'User Authentication Release',
        'description': 'Release milestone for the new user authentication feature.',
        'start_date': '2024-05-01',
        'due_date': '2024-07-11'
    })
